#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char f(size_t i) {
	if (i < 10)
		return i + '0';
	if (i < 36)
		return i + 55;
	return i + 61;
}

int main(int const c, char const *v[]) {
	static FILE *fi, *fo, *fk;
	static char key[20], text[100];

	fi = fopen("encrypted.txt", "r");
	fo = fopen("flag.txt", "w");
	fk = fopen("key.txt", "r");

	assert(fi);
	assert(fo);
	assert(fk);

	fgets(text, 69, fi);
	fclose(fi);

	fscanf(fk, "%s", key);
	fclose(fk);

	memmove(key + 4, key, strlen(key));
	
	char buffer[58];

	for (size_t a = 0; a < 62; a++)
	for (size_t b = 0; b < 62; b++)
	for (size_t c = 0; c < 62; c++)
	for (size_t d = 0; d < 62; d++) {
	  bool flag = true;
		key[0] = f(a);
		key[1] = f(b);
		key[2] = f(c);
		key[3] = f(d);
		srand(key[0] * key[1] * key[2] * key[3]);
		printf("\r%i %i", a,b);
		for (size_t i = 0; i < strlen(text); i++) {
			int r = rand();
			int z = r ^ key[r % strlen(key)] ^ text[i];
			if (isprint(z & 0xFF))
				//fputc((char) z, fo);
				buffer[i] = z;
		  else
		    flag = false;
		}
		if(flag)
		  fprintf(fo, "%s: %s\n", key, buffer);
	}

	fclose(fo);
}
